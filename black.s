
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"

frmcnt = localramstart+0

.segment "CODE"

black:
   inc frmcnt
   bne @notdone
   cli

@notdone:
   lda #$00
   sta COLUP0
   sta COLUP1
   sta COLUPF
   sta COLUBK
   sta AUDC0
   sta AUDC1
   sta AUDV0
   sta AUDV1

   jsr waitvblank
   jsr waitscreen
   jmp waitoverscan
