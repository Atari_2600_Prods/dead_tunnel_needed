
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"

pfline4:
   .res 4 ; pfline+1 = buffer
pfline2:
   .res 1
pfline3:
   .res 1
frameidx:
   .res 2
coloridx:
   .res 1
color:
   .res 4
temp:
   .res 2
charpos:
   .res 1
scroll:
   .res 82
sinoff:
   .res 1
textvec:
   .word 0


; o = bgcolor1
; x = pfcolor1
; O = bgcolor2
; X = bgcolor2
; 
; oooooooo  pf1 h=(6-i)
; oxxxxxxo  pf2 h=7
; oxOOOOxo  pf3 h=7
; oxOXXOxo  pf4 h=i
; oxOXXOxo  pf4 h=
; oxOOOOxo  pf3
; oxxxxxxo  pf2
; oooooooo  pf1

; pf
; 111111100000001111111
; 111111000000011111110
; 000000011111110000000
; total data
; 1111111000000011111110000000 = 28bit = 20 bif pf + 8 bit buffer
; l4=pf
; l3=
; 

.segment "RODATA"
text:
   .byte "          "
   .byte "EVERY DEMO COMPO NEEDS A TUNNEL. SO HERE IS A SIMPLE 4K ONESCREENER, "
   .byte "THAT'S BEEN CREATED WAY AFTER THE DEADLINE OF DEADLINE 2014. "
   .byte "MUSIC: SUNSPIRE, PLAYER: P.SLOCUM, CODE: SVOLLI. GREETINGS TO: ALL VCS CODERS, "
   .byte "KAOMAU, GASPODE, BRAIN CONTROL, CODE RED, DDK, G*P, RABENAUGE, TITAN, TRBL, "
   .byte "TRSI AND OF CAUSE ALL CONTRIBUTING TO DEADLINE.    "
   .byte "OH, I FOUND ANOTHER UNUSED EFFECT... DID YOU KNOW THAT THE VCS "
   .byte "ONLY HAS 2 8BIT SPRITES AND 3 1BIT SPRITES? HERE THEY ARE..."
   .byte "           ",0

p0sin:
   .byte $e0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $00
   .byte $f0
   .byte $f0
   .byte $00
   .byte $f0
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $10
   .byte $00
   .byte $10
   .byte $10
   .byte $00
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $20
   .byte $10
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $20
   .byte $10
   .byte $10
   .byte $20
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $10
   .byte $00
   .byte $10
   .byte $10
   .byte $00
   .byte $10
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $00
   .byte $f0
   .byte $00
   .byte $f0
   .byte $f0
   .byte $00
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0
   .byte $e0
   .byte $f0

colortab:
   .byte $10,$50,$10,$10,$10,$10,$10,$50
   .byte $10,$10,$10,$10,$10,$50,$10,$10
   .byte $10,$50,$10,$10,$10,$50,$10,$10
   .byte $10,$50,$10,$10,$50,$10,$10,$50
   .byte $10,$10,$50,$10,$50,$10,$50,$10
   .byte $50,$50,$50,$52,$52,$52
   .byte $54,$54,$54,$56,$56,$56
   .byte $58,$58,$5a,$5a
   .byte $5c,$5c,$5e,$5e
   .byte $20,$22,$24,$26,$28,$2a,$2c,$2e
   .byte $40,$42,$44,$46,$48,$4a,$4c,$4e
   .byte $60,$62,$64,$66,$68,$6a,$6c,$6e
   .byte $80,$82,$84,$86,$88,$8a,$8c,$8e
   .byte $a0,$a2,$a4,$a6,$a8,$aa,$ac,$ae
   .byte $c0,$c2,$c4,$c6,$c8,$ca,$cc,$ce
   .byte $d0,$d2,$d4,$d6,$d8,$da,$dc,$de
   .byte $b0,$b2,$b4,$b6,$b8,$ba,$bc,$be
   .byte $90,$92,$94,$96,$98,$9a,$9c,$9e
   .byte $70,$72,$74,$76,$78,$7a,$7c,$7e
   .byte $50,$52,$54,$56,$58,$5a,$5c,$5e
   .byte $30,$32,$34,$36,$38,$3a,$3c,$3e
colortabend:

.segment "CODE"

tunnel:
   lda color+3
   bne payload
   jsr init
   bne payload

init:
   lda #$01
   sta CTRLPF
   lda #$05
   sta NUSIZ0
   sta NUSIZ1
   lda colortab
   ldy #$08
   sty charpos
   sta color+3
   sta WSYNC
   jsr delay41
   sta RESP0
   sta WSYNC
   jsr delay41
   sta RESP1
   lda #$e1
   sta HMP1
   sta WSYNC
   sta HMOVE
   sta VDELP1
   lda #<text
   sta textvec
   lda #>text
   sta textvec+1

resetidx:
   lda #$02
   sta frameidx
   
   lda #%00000000
   sta pfline4+0
   lda #%00011111
   sta pfline4+1
   lda #%00000011
   sta pfline4+2
   lda #%01111111
   sta pfline4+3
   
   rts

payload:

   inc sinoff
   lda sinoff
   lsr
   tax
   lda p0sin,x
   sta HMP0
   sta HMP1
   sta WSYNC
   sta HMOVE

   lda #$00
   sta COLUPF
   ldy frameidx
   cpy #$03
   bcs @colorok
   ldx #$00
@copycol:
   lda color+1,x
   sta color+0,x
   inx
   cpx #$03
   bcc @copycol
   ldx coloridx
   inx
   cpx #<(colortabend-colortab)
   bne @nocolres
   ldx #$00
@nocolres:
   stx coloridx
   lda colortab,x
   sta color+3
@colorok:

   lda pfline4+1
   ora #%00011111
   sta pfline2
   lda pfline4+2
   and #%00000011
   sta pfline3

   lda frameidx
   and #$fe
   tay
   clc
   adc #$28
   sta frameidx+1
   ldx #$00 ; for sprite counter
   stx COLUP1
   lda #$0e
   sta COLUP0

.macro setgfx _pf0,_pf1,_pf2
   stx WSYNC
   lda scroll,x ; 4
   sta GRP0     ; 3
   lda _pf0     ; 3
   sta PF0      ; 3
   lda _pf1     ; 3
   sta PF1      ; 3
   lda _pf2     ; 3
   sta PF2      ; 3
                ; 25 / 16 bytes
.endmacro

.macro delaygfx
   stx WSYNC
   lda scroll,x ; 4
   sta GRP1     ; 3
   jsr delay18
.endmacro

.macro delaygfx1
   stx WSYNC
   lda scroll,x ; 4
   sta GRP1     ; 3
   jsr delay19
.endmacro

.macro line1 _step,_compare
.local @loop
@loop:
   setgfx #$00,#$00,#$00
   stx WSYNC
   lda scroll,x ; 4
   sta GRP1     ; 3
   stx WSYNC
   inx
.if .match ({_step}, +1)
   iny
   cpy _compare
   bcc @loop
.elseif .match ({_step}, -1)
   dey
   tya
   ora #$01
   cmp _compare
   bcs @loop
.else
.error "wrong step"
.endif
.endmacro

.macro line2 _step,_compare
.local @loop
@loop:
   setgfx pfline4+0,pfline2,#$ff
   stx WSYNC
   lda scroll,x ; 4
   sta GRP1     ; 3
   stx WSYNC
   inx
.if .match ({_step}, +1)
   iny
   cpy _compare
   bcc @loop
.elseif .match ({_step}, -1)
   dey
   cpy _compare
   bcs @loop
.else
.error "wrong step"
.endif
.endmacro

.macro line3 _step,_compare
.local @loop
@loop:
   setgfx pfline4+0,pfline4+1,pfline3
   lda color+2
   sta $2000+COLUBK
   jsr delay28
   lda color+0
   sta COLUBK
   delaygfx1
   lda color+2
   sta COLUBK
   jsr delay28
   lda color+0
   sta COLUBK
   delaygfx1
   lda color+2
   sta COLUBK
   inx
.if .match ({_step}, +1)
   iny
.elseif .match ({_step}, -1)
   dey
.endif
   jsr delay24
   lda color+0
   sta COLUBK
.if .match ({_step}, +1)
   cpy _compare
   bcc @loop
.elseif .match ({_step}, -1)
   cpy _compare
   bcs @loop
.else
.error "wrong step"
.endif
.endmacro

.macro line4 _step,_compare
.local @loop
@loop:
   setgfx pfline4+0,pfline4+1,pfline4+2
   lda color+2
   sta COLUBK
   bit $ff
   lda color+3
   sta COLUPF
   jsr delay12
   lda color+1
   sta COLUPF
   bit $ff     ; 3
   lda color+0
   sta COLUBK
   delaygfx
   lda color+2
   sta COLUBK
   bit $ff     ; 3
   lda color+3
   sta COLUPF
   jsr delay12
   lda color+1
   sta COLUPF
   bit $ff     ; 3
   lda color+0
   sta COLUBK
   delaygfx
   lda color+2
   sta COLUBK
   pha         ; 3
   lda color+3
   sta COLUPF  
   inx
.if .match ({_step}, +1)
   iny
.elseif .match ({_step}, -1)
   dey
.endif
   pla         ; 4
   bit $ff     ; 3
   lda color+1
   sta COLUPF
   bit $ff     ; 3
   lda color+0
   sta COLUBK
.if .match ({_step}, +1)
   cpy _compare
   bcc @loop
.elseif .match ({_step}, -1)
   cpy _compare
   bcs @loop
.else
.error "wrong step"
.endif
.endmacro

   lda color+0
   pha
   lda color+1
   sta COLUPF

   cpy #$06
   bcc tunnel3
   jmp tunnel4

tunnel3:
   jsr waitvblank
   pla
   sta WSYNC
   sta COLUBK
   line1 +1,#(8*2)
   line2 +1,#(15*2)
   line3 +1,frameidx+1
   line3 -1,#(15*2)
   jmp tunnelend
   
tunnel4:
   jsr waitvblank
   pla
   sta WSYNC
   sta COLUBK
   line1 +1,#(8*2)
   line2 +1,#(15*2)
   line3 +1,#(22*2)
   line4 +1,frameidx+1
   line4 -1,#(22*2)
   line3 -1,#(15*2)
tunnelend:
   line2 -1,#(8*2)
   line1 -1,frameidx

   sty WSYNC
   lda #$00
   sta GRP1
   sta GRP0
   sty WSYNC
   sta COLUBK
   sta PF0
   sta PF1
   sta PF2

   ldy charpos
   dey
   bpl @cpok
   ldy #$07
   inc textvec
   bne @cpok
   inc textvec+1
@cpok:
   sty charpos
   ldy #$00
   lda (textvec),y
   bne @notdone
   cli
@notdone:
   ldy charpos
   sec
   sbc #$20
   sta temp
   lda #$00
   sta temp+1
   asl temp
   rol temp+1
   asl temp
   rol temp+1
   asl temp
   rol temp+1
   lda temp
   adc #<(charset)
   sta temp
   lda temp+1
   adc #>(charset)
   sta temp+1
   lda (temp),y
   sta scroll+81

   clc
   lda frameidx
   adc #$01
   cmp #$10 ; overflow, restart
   bcs @overflow
   sta frameidx
   and #$01
   bne @nooverflow
   asl pfline4+3
   ror pfline4+2
   rol pfline4+1
   ror pfline4+0
   bcc @nooverflow
@overflow:
   jsr resetidx
@nooverflow:

   ldy #$00
   lda (textvec),y
   sta temp
   jsr waitscreen
   jsr psmkPlayer

   lda temp
   bne @copy
   jmp @nocopy
   
@copy:
.if 1
   ; speedcode needed when using music
.macro copyup _index
   lda scroll+_index+1
   sta scroll+_index
.endmacro
   copyup  0
   copyup  1
   copyup  2
   copyup  3
   copyup  4
   copyup  5
   copyup  6
   copyup  7
   copyup  8
   copyup  9
   copyup 10
   copyup 11
   copyup 12
   copyup 13
   copyup 14
   copyup 15
   copyup 16
   copyup 17
   copyup 18
   copyup 19
   copyup 20
   copyup 21
   copyup 22
   copyup 23
   copyup 24
   copyup 25
   copyup 26
   copyup 27
   copyup 28
   copyup 29
   copyup 30
   copyup 31
   copyup 32
   copyup 33
   copyup 34
   copyup 35
   copyup 36
   copyup 37
   copyup 38
   copyup 39
   copyup 40
   copyup 41
   copyup 42
   copyup 43
   copyup 44
   copyup 45
   copyup 46
   copyup 47
   copyup 48
   copyup 49
   copyup 50
   copyup 51
   copyup 52
   copyup 53
   copyup 54
   copyup 55
   copyup 56
   copyup 57
   copyup 58
   copyup 59
   copyup 60
   copyup 61
   copyup 62
   copyup 63
   copyup 64
   copyup 65
   copyup 66
   copyup 67
   copyup 68
   copyup 69
   copyup 70
   copyup 71
   copyup 72
   copyup 73
   copyup 74
   copyup 75
   copyup 76
   copyup 77
   copyup 78
   copyup 79
   copyup 80
.else
   ldx #$00
@moveloop:
   lda scroll+1,x
   sta scroll,x
   inx
   cpx #81
   bne @moveloop
.endif
@nocopy:

   jmp waitoverscan

delay41:
   .byte $c9
delay40:
   .byte $c9
delay39:
   .byte $c9
delay38:
   .byte $c9
delay37:
   .byte $c9
delay36:
   .byte $c9
delay35:
   .byte $c9
delay34:
   .byte $c9
delay33:
   .byte $c9
delay32:
   .byte $c9
delay31:
   .byte $c9
delay30:
   .byte $c9
delay29:
   .byte $c9
delay28:
   .byte $c9
delay27:
   .byte $c9
delay26:
   .byte $c9
delay25:
   .byte $c9
delay24:
   .byte $c9
delay23:
   .byte $c9
delay22:
   .byte $c9
delay21:
   .byte $c9
delay20:
   .byte $c9
delay19:
   .byte $c9
delay18:
   .byte $c9
delay17:
   .byte $c9
delay16:
   .byte $c9
delay15:
   .byte $c5
delay14:
   nop
delay12:
   rts
