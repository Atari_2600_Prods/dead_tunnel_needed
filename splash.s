
.include "vcs.inc"
.include "globals.inc"

.segment "RODATA"

.if splashtype = 3

ntscpaltable:
; eor table
  .byte $00 ; $0x
  .byte $30 ; $2x
  .byte $00 ; $2x
  .byte $70 ; $4x
  .byte $20 ; $6x
  .byte $D0 ; $8x
  .byte $C0 ; $Ax
  .byte $B0 ; $Cx
  .byte $50 ; $Dx
  .byte $20 ; $Bx
  .byte $30 ; $9x
  .byte $C0 ; $7x
  .byte $90 ; $5x
  .byte $E0 ; $3x
  .byte $D0 ; $3x
  .byte $D0 ; $2x

.endif

.if splashtype = 4

ntscpaltable:
; ora table
  .byte $00
  .byte $20
  .byte $20
  .byte $40
  .byte $60
  .byte $80
  .byte $A0
  .byte $C0
  .byte $D0
  .byte $B0
  .byte $90
  .byte $70
  .byte $50
  .byte $30
  .byte $30
  .byte $20

.endif

.segment "ZEROPAGE"

frmcnt = $8f

; gfx are written to $90-$ec

.segment "CODE"

.if splashtype > 0

splash:
   ;ldy #$00 ; already cleaned by @gfxloop
   lda frmcnt
   bne @noinit
   lda #$01
   sta CTRLPF
   asl
   sta AUDC0
   ldx #$1f   ; X on purpose, will be used in copy below
   stx AUDF0
   lda #$fc
   sta AUDV0
   sta frmcnt

   lda #%00110000
@gfxloop:   
   tay
   ora #$80
@loop1:
   sta $71,x  ; copy to $90-$ec, X starts with 1, see above
   inx
   dey
   bne @loop1 ; from here on Y=$00, assumed below
   and #$7f
   lsr
   cmp #%00000001
   bne @gfxloop

@noinit:
   cmp #$fb
   bne @notmagicframe
   bit INPT4
   bpl @waitbuttonup
@notmagicframe:
   dec frmcnt
@waitbuttonup:
   jsr waitvblank
   ldx frmcnt
   bne @notdone
   
   cli
   beq @exit
@notdone:
   cpx #$10
   bcs @nomute
   sty AUDV0 ; Y should be $00 here
@nomute:
   cpx #$50
   bcs @disploop
   lda #$06 
   sta AUDC0
   txa
   ldx #$50
   sbc #$2f
   bpl @norev
   eor #$1f
@norev:
   sta AUDF0
   
@disploop:
   cpx #$90
   bcc @cleargfx
   lda $00,x
   .byte $24
@cleargfx:
   tya
   sta WSYNC
   sta PF2
   txa

.if splashtype = 1

   asl

.elseif splashtype = 2

; 2 %0010 <- 2 %0010
; 4 %0100 <- 3 %0011
; 6 %0110 <- 4 %0100
; 8 %1000 <- 5 %0101
; A %1010 <- 6 %0110
; C %1100 <- 7 %0111
   and #$07
   sta $82
   txa
   asl ; a > $80 => sec
   bmi @upper
   and #$70
   sbc #$10
   bpl @skip
@upper:
; D %1101 <- 8 %1000
; B %1011 <- 9 %1001
; 9 %1001 <- A %1010
; 7 %0111 <- B %1011
; 5 %0101 <- C %1100
; 3 %0011 <- D %1101
   and #$70
   eor #$ff
   adc #$68
@skip:   
   ora $82
   asl

.elseif splashtype = 3

   lsr
   lsr
   lsr
   and #$0f
   tay
   txa
   asl
   eor ntscpaltable,y
   ldy #$00

.elseif splashtype = 4

   lsr
   lsr
   lsr
   and #$0f
   tay
   txa
   asl
   and #$0f
   ora ntscpaltable,y
   ldy #$00
   
.endif

   sta COLUPF
   inx
   cpx #$ee ; last byte = $00 to clear PF2
   bcc @disploop

@done:
   jsr waitscreen
   jmp waitoverscan

@exit:
   txa
   ldx #$81
@clrloop:
   sta $ff,x
   inx
   bne @clrloop
   jmp @done
   
.endif
